<?php

/**
 * Created by PhpStorm.
 * User: bsn
 * Date: 28.10.18
 * Time: 13:44
 */


namespace Burikovs\XDemolist\Pub\Controller;

use DateTime;

use JsonException;
use XF\Mvc\ParameterBag;
use XF\Pub\Controller\AbstractController;

class Demolist extends AbstractController
{

    const DEMO_URL = 'http://csgo.g-nation.ru/';

    public function actionIndex(ParameterBag $params)
    {
        $viewParams = array('demoList' => $this->getDemo());

        return $this->view('', 'demo_list', $viewParams);
    }


    /**
     * ������ ������ ������.  �������� ������ �����. �� ������ �������� ������ � ����������� ������
     *  <li><strong>mtime</strong> - ����� � �������� ����, ������� ������ Nginx</li>
     * <li><strong>stamp</strong> - timestamp, �� ���� �� ����� ��������� ������ �� ������ � �������</li>
     * <li><strong>filePath</strong> - ������ ���� �� ����� �����</li>
     * @return array|bool
     */
    private function getDemo(): bool|array
    {
        if ( ! $this->getCurlData()) {
            return false;
        }

        $files = $this->getCurlData();
        foreach ($files as $iValue) {
            if ($iValue['type'] === 'directory' || ! preg_match('/.*dem$/ui', strtolower($iValue['name']), $match)) {
                continue;
            }
            $date = new DateTime($iValue['mtime']);
            $result[] = array(
                'mtime' => $iValue['mtime'],
                'stamp' => $date->getTimestamp(),
                'filePath' => self::DEMO_URL.$iValue['name'],
                'baseName' => $iValue['name'],
            );
        }

        usort(
            $result,
            static function ($a, $b) {
                return ($b['stamp'] - $a['stamp']);
            }
        );

        return $result;
    }

    /**
     * ������������ � Nginx, ��� ����� ������ ����� � ������� JSON.
     * @return mixed
     * @throws JsonException
     */
    private function getCurlData(): mixed
    {
        $curl = curl_init(self::DEMO_URL);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($curl);
        curl_close($curl);
        if ( ! $data || json_last_error()) {
            return false;
        }

        return json_decode($data, true, 512, JSON_THROW_ON_ERROR);
    }

}